<?php if (isset($errormsg)) {
    echo $errormsg;
} else {
    if (isset($form_css_button)) {
        echo "<style>".$form_css_button."</style>";
    } ?>
    <form action="<?php echo $payUrl ?>">
        <input type="submit" value="<?php echo $buttonLabel ?>" />
    </form>
<?php } ?>
