<?php

class ControllerExtensionPaymentCapusta extends Controller
{
    const HEADER_OK = "HTTP/1.0 200 OK";
    const HEADER_BAD_REQUEST = "HTTP/1.0 400 Bad Request";

    const CALLBACK_DATA = 'amount';
    const CALLBACK_AMOUNT = 'amount';
    const CALLBACK_STATUS = 'status';
    const CALLBACK_TRANSACTIONID = 'transactionId';

    const CALLBACK_STATUS_PAID = 'SUCCESS';
    const CALLBACK_STATUS_FAILED = 'FAIL';
    const CAPUSTA_SERVER = "3.123.194.134";
    private $configPrefix;

    private function getPrefix()
    {
        if ($this->configPrefix === null) {
            $this->configPrefix = '';
            if (version_compare(VERSION, '3.0') >= 0) {
                $this->configPrefix = 'payment_';
            } else {
                $this->configPrefix = '';
            }
        }
        return $this->configPrefix;
    }

    public function index()
    {
        $data['button_confirm'] = $this->language->get('button_confirm');
        $this->load->language('extension/payment/capusta');
        $this->load->model('checkout/order');
        $this->load->model('extension/payment/capusta');
        $responseError = false;
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        try {
            $responseCreateInvoice = $this->model_extension_payment_capusta->createInvoice($order_info);
            $responseError = json_decode($responseCreateInvoice['error'], true);
            $response = json_decode($responseCreateInvoice['body'], true);
            $this->model_checkout_order->addOrderHistory(
                $this->session->data['order_id'],
                $this->config->get($this->getPrefix().'capusta_order_status_progress_id')
            );
        } catch (Exception $ex) {
            $logs = array();
            $logs['error']['message'] = $ex->getMessage();
            $data['errormsg'] = $ex->getMessage();
            $this->model_extension_payment_capusta->logger('exception', $logs);
            $responseError = $data['errormsg'];
        }
        if (!isset($response['payUrl'])) {
                $logs = array();
                if (isset($response['error'])) {
                    $error = $response['error'];
                } else {
                    if (isset($response['message'])) {
                        $error = $response['message'];
                    }
                }
                $logs['error'] = $responseError = $error;
                $this->model_extension_payment_capusta->logger('error', $logs);
        }
        $data['buttonLabel'] = $this->config->get($this->getPrefix().'capusta_button_label');
        if (!$data['buttonLabel']) $data['buttonLabel']  = $data['button_confirm'];
        if ($responseError || !isset($response['payUrl'])) {
            $data['errormsg'] = $this->language->get('text_paymentError');
        } else {
            // no errors, registering created payment.
            $this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($this->getPrefix().'config_order_status_id'));
            $data['payUrl'] = $response['payUrl'];

        }
        return $this->load->view('extension/payment/capusta', $data);
    }

    public function success()
    {
        $this->response->redirect($this->url->link('checkout/success', '', true));

        return true;
    }

    public function fail()
    {

        $this->response->redirect($this->url->link('checkout/checkout', '', true));

        return true;
    }

    public function callback() {
        $this->getPrefix();
        $method = __METHOD__;

        $content = file_get_contents('php://input');
        $header = $this->getAuthHeader();
        if ($header) $header = 'Authorization: '.$header;

        $logs = array(
            'request' => array(
                'authHeader' => $header,
                'method' => 'POST',
                'data' => $content,
            ),
        );
        $data = json_decode($content, TRUE);

        $this->load->language('extension/payment/capusta');
        $this->load->model('extension/payment/capusta');
        $this->load->model('checkout/order');

        $callbackOrderStatus = $data[static::CALLBACK_STATUS];
        $logs['callbackOrderStatus'] = $callbackOrderStatus;
        $orderData = explode('-',$data[static::CALLBACK_TRANSACTIONID]);
        if (isset($orderData[1])) {
            $callbackOrderId = $orderData[1];
        } else {
            $callbackOrderId = $data[static::CALLBACK_TRANSACTIONID];
        }
        $modelBearer = $this->model_extension_payment_capusta->getBearer();
        if ($header) {
            if ($modelBearer !== $header) {
                $logs['error']['message'] = 'headerBearer: '.$header. ' ModelBearer: '.$modelBearer;
                $logs['error']['message'] .= '; Callback notification Bearer is incorrect ';
                return $this->outputWithLogger($method, $logs, $logs['error']['message']);
            }
        } else {
            // no headers received. Performing only ip check.
            if ($_SERVER['REMOTE_ADDR'] !== self::CAPUSTA_SERVER) {
                return $this->outputWithLogger($method, $logs, 'IP CHECK FAILED AND AUTH HEADERS NOT SET');
            } else {
                $logs['checkIpStatus'] = 'OK';
            }
        }

        if (!$order_info = $this->model_checkout_order->getOrder($callbackOrderId)) {
            $logs['error']['message'] = 'Order ' . $callbackOrderId . ' is missing';
            return $this->outputWithLogger($method, $logs, $logs['error']['message']);
        }
        $logs['order_info'] = $order_info;
        if (!empty($order_info['total'])) {
            $order_amount = (int)$this->model_extension_payment_capusta->prepareAmount($order_info['total']);
            $invoice_amount = (int)$data[static::CALLBACK_DATA][static::CALLBACK_AMOUNT];
            if ($order_amount != $invoice_amount) {
//                $logs['order_info'] = $order_info;
                $logs['error']['message'] = 'Received amount ' . $order_amount . ' vs Order amount mismatch ' .$invoice_amount;
                return $this->outputWithLogger($method, $logs, $logs['error']['message']);
            }
        } else {
            $logs['error']['message'] = 'order_info[total] is empty.';
            return $this->outputWithLogger($method, $logs, $logs['error']['message']);
        }
        //changing order status.
        if ($callbackOrderStatus == static::CALLBACK_STATUS_PAID) {
            $logs['status'][] = 'got order success';

            $this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($this->getPrefix().'capusta_order_status_success_id'), $this->config->get('config_name'), TRUE);
        }

        if ($callbackOrderStatus == static::CALLBACK_STATUS_FAILED) {
            $logs['status'][] = 'got order failed';
            $this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get($this->getPrefix().'capusta_order_status_cancelled_id'), $this->config->get('config_name'), TRUE);
        }
        return $this->outputWithLogger($method, $logs, 'OK', self::HEADER_OK);

    }

    private function outputWithLogger($method, &$logs, $message, $header = self::HEADER_BAD_REQUEST)
    {
        $response = array('message' => $message);
        $this->load->model('extension/payment/capusta');
        $this->model_extension_payment_capusta->logger($method, $logs);
        $this->response->addHeader($header);
        $this->response->setOutput(json_encode($response));
    }

    private function getAuthHeader() {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        if (isset($headers['Authorization'])) return $headers['Authorization'];
        return false;
    }
}

class ControllerPaymentCapusta extends ControllerExtensionPaymentCapusta
{

}