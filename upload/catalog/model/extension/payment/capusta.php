<?php

class ModelExtensionPaymentCapusta extends Model
{
    /**
     * Create invoice settings
     */

    private $api_url = 'https://api.capusta.space/v1/partner'; //PRODUCTIVE
    private $test_api_url = 'https://api.stage.capusta.space/v1/partner'; //TEST
    private $configPrefix;

    private function getPrefix()
    {
        if ($this->configPrefix === null) {
            $this->configPrefix = '';
            if (version_compare(VERSION, '3.0') >= 0) {
                $this->configPrefix = 'payment_';
            } else {
                $this->configPrefix = '';
            }
        }
        return $this->configPrefix;
    }

    public function getMethod($address, $total)
    {
        $this->load->language('extension/payment/capusta');

        if ($this->config->get($this->getPrefix().'capusta_status')) {

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get($this->getPrefix().'capusta_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

            if (!$this->config->get($this->getPrefix().'capusta_geo_zone_id')) {
                $status = TRUE;
            } elseif ($query->num_rows) {
                $status = TRUE;
            } else {
                $status = FALSE;
            }
        } else {
            $status = FALSE;
        }

        $method_data = array();

        if ($status) {
            $method_data = array(
                'code' => 'capusta',
                'title' => $this->language->get('text_title'),
                'terms' => '',
                'sort_order' => $this->config->get($this->getPrefix().'capusta_sort_order')
            );
        }
        return $method_data;
    }

    private function getHeaders()
    {
        $headers = array();
        $headers[] = 'X-Request-ID: ' . uniqid();
        $headers[] = $this->getBearer();
        $headers[] = 'Content-type: application/json; charset=utf-8';
        $headers[] = 'Accept: application/json';
        return $headers;
    }


    public function getBearer()
    {
        return 'Authorization: Bearer ' . $this->config->get($this->getPrefix().'capusta_merchant_email').':'.$this->config->get($this->getPrefix().'capusta_merchant_token');
    }

    public function createInvoice(array $order_info)
    {
        $data = [
            'id' => substr($this->config->get($this->getPrefix().'capusta_mode'),0,1).date("Ymd")."-".$order_info['order_id'],
            'amount' => [
                'amount' => $this->currency->format($order_info['total'], $order_info['currency_code'], false) * 100,
                'currency' => $order_info['currency_code'],
            ],
            'projectCode' => $this->config->get($this->getPrefix().'capusta_merchant_projectCode'),
            'description' => $this->getProductDescription(),
        ];

        $url = $this->prepareApiUrl('payment');
        $headers = $this->getHeaders();
        return $this->send($url, 'POST', $headers, json_encode($data, true), 'init_invoice');
    }

    private function getRate($rate)
    {
        switch ($rate) {
            case '0':
            case '0.0000':
                return '0%';
                break;
            case '10':
            case '10.0000':
                return '10%';
                break;
            case '18':
            case '18.0000':
                return '18%';
                break;
            case '20':
            case '20.0000':
                return '20%';
                break;
            case '10/100':
                return '10/110';
                break;
            case '18/118':
                return '18/118';
                break;
            case '20/120':
                return '20/120';
                break;
            default:
                return null;
                break;
        }
    }

    private function getProductDescription()
    {
        $products = '';

        $i = 0;
        foreach ($this->cart->getProducts() as $product) {
            if ($i == 0) {
                $products .= $product['quantity'] . ' x ' . $product['name'];
            } else {
                $products .= ', ' . $product['quantity'] . ' x ' . $product['name'];
            }
            $i++;
        }

        if (mb_strlen($products, 'UTF-8') > 255) {
            $products = mb_substr($products, 0, 252, 'UTF-8') . '...';
        }

        return $products;
    }

    private function send($url, $method = "POST", $headers = array(), $data = '', $type = '')
    {
        $logs = array(
            'request' => array(
                'url' => $url,
                'method' => $method,
                'headers' => $headers,
                'data' => $data,
            ),
        );
        $this->logger($type . ': request', $logs);

        if (empty($url)) {
            throw new Exception('Required url parameter not passed');
        }

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_USERAGENT, "OpenCart " . VERSION);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $body = curl_exec($curl);
        $info = curl_getinfo($curl);
        $curl_errno = curl_errno($curl);

        $response['http_code'] = $info['http_code'];
        $response['body'] = $body;
        $response['error'] = $curl_errno;

        $logs['response'] = $response;

        $this->logger($type . ': response', $logs);

        curl_close($curl);

        return $response;
    }

    /**
     * Prepare amount (e.g. 124,24 -> 12424)
     *
     * @param $amount int
     * @return int
     */
    public function prepareAmount($amount)
    {
        return number_format($amount, 2, '.', '') * 100;
    }

    private function prepareApiUrl($path = '', $query_params = array())
    {
        $mode = $this->config->get($this->getPrefix().'capusta_mode');
        if ($mode == 'test') {
            $url = rtrim($this->test_api_url, '/') . '/' . $path;
        } else {
            $url = rtrim($this->api_url, '/') . '/' . $path;
        }
        if (!empty($query_params)) {
            $url .= '?' . http_build_query($query_params);
        }
        return $url;
    }

    public function logger($method, $message)
    {
        $this->log->write('Capusta ' . $method . '. ' . print_r($message, true));
    }

}
