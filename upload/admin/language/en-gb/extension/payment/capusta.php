<?php
// Heading
$_['heading_title'] = 'Capusta.Space';

// Text
$_['text_extension'] = 'Extensions';
$_['text_all_zones'] = 'All Geo Zones';
$_['text_success'] = 'Success: You have modified Capusta.Space account details!';
$_['text_edit'] = 'Edit Capusta.Space';
$_['text_capusta'] = '<a target="_BLANK" href="https://capusta.space/"><img src="view/image/payment/capusta.png" alt="Capusta.Space Website" title="Capusta.Space Website" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live'] = 'Live';
$_['text_sandbox'] = 'Sandbox';
$_['text_enabled'] = 'Enabled';
$_['text_disabled'] = 'Disabled';
$_['text_prod'] = 'Production';
$_['text_test'] = 'Test';
$_['text_home'] = 'Home';
$_['text_buttonLabel'] = 'Pay by card';
$_['button_confirm'] = "Pay by Card";


// Entry
$_['entry_mode'] = 'Gateway Mode';
$_['entry_merchant_email'] = 'Merchant Email';
$_['entry_merchant_token'] = 'Merchant Token';
$_['entry_merchant_projectCode'] = 'ID Project (projectCode)';
$_['entry_form_button_label'] = 'Button label in payment form:';
$_['entry_order_status'] = 'Order status after creation:';
$_['entry_order_status_success'] = 'Order status if payment success:';
$_['entry_order_status_progress'] = 'Order Status after invoicing:';
$_['entry_order_status_cancelled'] = 'Order Status if payment failed or cancelled:';
$_['entry_geo_zone'] = 'Geo Zone';
$_['entry_status'] = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_notify_url'] = 'Webhooks URL';
$_['entry_success_url'] = 'Success URL';
$_['entry_fail_url'] = 'Fail URL';
// Help
$_['help_form_button_label'] = 'Your button label for payment form';
$_['help_capusta_mode'] = 'To use test mode you would have merchant and project] created on test environment of Capusta service ';
$_['help_capusta_setup'] = '<a target="_blank" href="https://bitbucket.org/capustaspace/capusta-opencart23/src/master/">Click here</a> to learn how to set up Capusta.Space';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Capusta.Space payment!';
$_['error_merchant_email'] = 'Merchant Email required!';
$_['error_merchant_token'] = 'Merchant Token required!';
$_['error_merchant_projectCode'] = 'ID Project required!';

