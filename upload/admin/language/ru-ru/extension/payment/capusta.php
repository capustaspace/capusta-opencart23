<?php
// Heading
$_['heading_title']                  = 'Capusta.Space';

// Text
$_['text_extension']                 = 'Расширения ';
$_['text_all_zones']                 = 'Все географические зоны';
$_['text_success']                   = 'Вы успешно изменили настройки!';
$_['text_edit']                      = 'Редактировать Capusta.Space';
$_['text_capusta']                   = '<a target="_BLANK" href="https://capusta.space/"><img src="view/image/payment/capusta.png" alt="Сайт Capusta.Space " title="Сайт Capusta.Space" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']                      = 'Продакшн';
$_['text_sandbox']                   = 'Песочница';
$_['text_enabled']                   = 'Включен';
$_['text_disabled']                  = 'Выключен';
$_['text_prod']                      = 'Боевой';
$_['text_test']                      = 'Тестовый';
$_['text_home']						 = 'Домой';
$_['text_buttonLabel']               = 'Оплатить картой';
$_['button_confirm'] = "Pay by Card";

// Entry
$_['entry_merchant_email']           = 'Email Мерчанта';
$_['entry_merchant_token']           = 'Токен Мерчанта';
$_['entry_merchant_projectCode']     = 'ID Проекта (projectCode)';
$_['entry_mode']                     = 'Режим шлюза';
$_['entry_form_button_label']        = 'Название кнопки на платежной форме:';
$_['entry_order_status']             = 'Статус заказа после cоздания:';
$_['entry_order_status_success']     = 'Статус заказа после успешной оплаты:';
$_['entry_order_status_progress']    = 'Статус заказа после выставления счета:';
$_['entry_order_status_cancelled']   = 'Статус заказа после неуспешной оплаты:';
$_['entry_geo_zone']                 = 'Географическая зона';
$_['entry_status']                   = 'Статус:';
$_['entry_sort_order']               = 'Порядок сортировки';
$_['entry_notify_url']				 = 'URL для оповещения о платеже:';
$_['entry_success_url']				 = 'URL для перенаправления после успешного платежа';
$_['entry_fail_url']				 = 'URL для перенаправления после неуспешного платежа';
// Help
$_['help_form_button_label'] 		= 'Надпись на кнопке в платежной форме';
$_['help_capusta_mode']             = 'Для работы в тестовом режиме мерчант и проект должны быть созданы в тестовом окружении сервиса Capusta';
$_['help_capusta_setup']            = '<a target="_blank" href="https://bitbucket.org/payterra/capusta-ocmod/src/master/">Click here</a> to learn how to set up Capusta.Space';

// Error
$_['error_permission']              = 'Внимание: У вас нет разрешений на изменение настроек Capusta.Space!';
$_['error_merchant_email']          = 'Email Мерчанта обязателен!';
$_['error_merchant_token']          = 'Токен Мерчанта обязателен!';
$_['error_merchant_projectCode']    = 'ID Проекта обязателен!';