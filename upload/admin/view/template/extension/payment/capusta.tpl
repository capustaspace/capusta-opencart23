<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-payment" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-payment" class="form-horizontal">
                    <div class="tab-content">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                            <div class="col-sm-10">
                                <select name="capusta_status" id="input-status" class="form-control">
                                    <?php if ($capusta_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-mode"><span data-toggle="tooltip" title="<?php echo $help_capusta_mode; ?>"><?php echo $entry_mode; ?></span></label>
                            <div class="col-sm-10">
                                <select name="capusta_mode" id="input-mode" class="form-control">
                                    <?php if ($capusta_mode == 'prod') { ?>
                                        <option value="prod" selected="selected"><?php echo $text_prod; ?></option>
                                        <option value="test"><?php echo $text_test; ?></option>
                                    <?php } else { ?>
                                        <option value="prod"><?php echo $text_prod; ?></option>
                                        <option value="test" selected="selected"><?php echo $text_test; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-merchant-email"><?php echo $entry_merchant_email; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="capusta_merchant_email" value="<?php echo $capusta_merchant_email; ?>" placeholder="<?php echo $entry_merchant_email; ?>" id="input-form-button-label" class="form-control"/>
                                <?php if ($error_merchant_email) { ?>
                                    <div class="text-danger"><?php echo $error_merchant_email; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-merchant-token"><?php echo $entry_merchant_token; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="capusta_merchant_token" value="<?php echo $capusta_merchant_token; ?>" placeholder="<?php echo $entry_merchant_token; ?>" id="input-form-button-label" class="form-control"/>
                                <?php if ($error_merchant_token) { ?>
                                    <div class="text-danger"><?php echo $error_merchant_token; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-merchant-projectCode"><?php echo $entry_merchant_projectCode; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="capusta_merchant_projectCode" value="<?php echo $capusta_merchant_projectCode; ?>" placeholder="<?php echo $entry_merchant_projectCode; ?>" id="input-form-button-label" class="form-control"/>
                                <?php if ($error_merchant_projectCode) { ?>
                                    <div class="text-danger"><?php echo $error_merchant_projectCode; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-notify-url"><?php echo $entry_notify_url; ?></label>
                            <div class="col-sm-10">
                                <input type="text" readonly value="<?php echo $notify_url; ?>" id="input-notify-url" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-success-url"><?php echo $entry_success_url; ?></label>
                            <div class="col-sm-10">
                                <input type="text" readonly value="<?php echo $success_url; ?>" id="input-success-url" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-fail-url"><?php echo $entry_fail_url; ?></label>
                            <div class="col-sm-10">
                                <input type="text" readonly value="<?php echo $fail_url; ?>" id="input-fail-url" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-order-status-progress"><?php echo $entry_order_status_progress; ?></label>
                            <div class="col-sm-10">
                                <select name="capusta_order_status_progress_id" id="input-order-status-progress" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $capusta_order_status_progress_id) { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-order-status-success"><?php echo $entry_order_status_success; ?></label>
                            <div class="col-sm-10">
                                <select name="capusta_order_status_success_id" id="input-order-status-success" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $capusta_order_status_success_id) { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-order-status-cancelled"><?php echo $entry_order_status_cancelled; ?></label>
                            <div class="col-sm-10">
                                <select name="capusta_order_status_cancelled_id" id="input-order-status-cancelled" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $capusta_order_status_cancelled_id) { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-form-button-label"><span data-toggle="tooltip" title="<?php echo $help_form_button_label; ?>"><?php echo $entry_form_button_label; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="capusta_button_label" value="<?php echo $capusta_button_label; ?>" placeholder="<?php echo $entry_form_button_label; ?>" id="input-form-button-label" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
                            <div class="col-sm-10">
                                <select name="capusta_geo_zone_id" id="input-geo-zone" class="form-control">
                                    <option value="0"><?php echo $text_all_zones; ?></option>
                                    <?php foreach ($geo_zones as $geo_zone) { ?>
                                        <?php if ($geo_zone['geo_zone_id'] == $capusta_geo_zone_id) { ?>
                                            <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="capusta_sort_order" value="<?php echo $capusta_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="alert alert-info"><?php echo $help_capusta_setup; ?></div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>