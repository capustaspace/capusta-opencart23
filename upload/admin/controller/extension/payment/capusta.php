<?php
class ControllerExtensionPaymentCapusta extends Controller {
    private $error = array();
    private $default_mode = 'prod';
    private $configPrefix;
    private function getPrefix()
    {
        if ($this->configPrefix === null) {
            $this->configPrefix = '';
            if (version_compare(VERSION, '3.0') >= 0) {
                $this->configPrefix = 'payment_';
            } else {
                $this->configPrefix = '';
            }
        }
        return $this->configPrefix;
    }
    
    public function index() {
        $this->load->language('extension/payment/capusta');

        if (method_exists($this->document, 'setTitle')) {
            $this->document->setTitle($this->language->get('heading_title'));
        } else {
            $this->document->title = $this->language->get('heading_title');
        }
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_prod'] = $this->language->get('text_prod');
        $data['text_test'] = $this->language->get('text_test');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['help_form_button_label'] = $this->language->get('help_form_button_label');
        $data['help_capusta_mode'] = $this->language->get('help_capusta_mode');
        $data['text_all_zones'] = $this->language->get('text_all_zones');

        $this->load->model('setting/setting');
        if (isset($this->session->data['token'])) {
            //chars for v2.3
            $data['heading_title'] = $this->language->get('heading_title');
            $data['button_save'] = $this->language->get('button_save');
            $data['button_cancel'] = $this->language->get('button_cancel');
            $data['entry_status'] = $this->language->get('entry_status');
            $data['entry_mode'] = $this->language->get('entry_mode');
            $data['entry_merchant_email'] = $this->language->get('entry_merchant_email');
            $data['entry_merchant_token'] = $this->language->get('entry_merchant_token');
            $data['entry_merchant_projectCode'] = $this->language->get('entry_merchant_projectCode');
            $data['entry_notify_url'] = $this->language->get('entry_notify_url');
            $data['entry_success_url'] = $this->language->get('entry_success_url');
            $data['entry_fail_url'] = $this->language->get('entry_fail_url');
            $data['entry_order_status_progress'] = $this->language->get('entry_order_status_progress');
            $data['entry_order_status_success'] = $this->language->get('entry_order_status_success');
            $data['entry_order_status_cancelled'] = $this->language->get('entry_order_status_cancelled');
            $data['entry_form_button_label'] = $this->language->get('entry_form_button_label');
            $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
            $data['entry_sort_order'] = $this->language->get('entry_sort_order');
            $data['help_capusta_setup'] = $this->language->get('help_capusta_setup');

        }
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting($this->getPrefix().'capusta', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            if (isset($this->session->data['token'])) {
                $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true));
            } else {
                $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
            }
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }


        if (isset($this->error['merchant_email'])) {
            $data['error_merchant_email'] = $this->error['merchant_email'];
        } else {
            $data['error_merchant_email'] = '';
        }

        if (isset($this->error['merchant_token'])) {
            $data['error_merchant_token'] = $this->error['merchant_token'];
        } else {
            $data['error_merchant_token'] = '';
        }

        if (isset($this->error['merchant_projectCode'])) {
            $data['error_merchant_projectCode'] = $this->error['merchant_projectCode'];
        } else {
            $data['error_merchant_projectCode'] = '';
        }

        $data['breadcrumbs'] = array();

        if (isset($this->session->data['user_token'])) {
            // v3.x
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_extension'),
                'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('extension/payment/capusta', 'user_token=' . $this->session->data['user_token'], true)
            );

            $data['action'] = $this->url->link('extension/payment/capusta', 'user_token=' . $this->session->data['user_token'], true);
            $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);
        } else {
            // v2.3
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_extension'),
                'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true)
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('extension/payment/capusta', 'token=' . $this->session->data['token'], true)
            );
            $data['action'] = $this->url->link('extension/payment/capusta', 'token=' . $this->session->data['token'], true);
            $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true);
        }


        if (isset($this->request->post[$this->getPrefix().'capusta_merchant_email'])) {
            $data[$this->getPrefix().'capusta_merchant_email'] = $this->request->post[$this->getPrefix().'capusta_merchant_email'];
        } else {
            if ($this->config->get($this->getPrefix().'capusta_merchant_email')) {
                $data[$this->getPrefix().'capusta_merchant_email'] = $this->config->get($this->getPrefix().'capusta_merchant_email');
            } else {
                $data[$this->getPrefix().'capusta_merchant_email'] = '';
            }
        }

        if (isset($this->request->post[$this->getPrefix().'capusta_mode'])) {
            $data[$this->getPrefix().'capusta_mode'] = $this->request->post[$this->getPrefix().'capusta_mode'];
        } else {
            if ($this->config->get($this->getPrefix().'capusta_mode')) {
                $data[$this->getPrefix().'capusta_mode'] = $this->config->get($this->getPrefix().'capusta_mode');
            } else {
                $data[$this->getPrefix().'capusta_mode'] = $this->default_mode;
            }
        }


        if (isset($this->request->post[$this->getPrefix().'capusta_merchant_projectCode'])) {
            $data[$this->getPrefix().'capusta_merchant_projectCode'] = $this->request->post[$this->getPrefix().'capusta_merchant_projectCode'];
        } else {
            if ($this->config->get($this->getPrefix().'capusta_merchant_projectCode')) {
                $data[$this->getPrefix().'capusta_merchant_projectCode'] = $this->config->get($this->getPrefix().'capusta_merchant_projectCode');
            } else {
                $data[$this->getPrefix().'capusta_merchant_projectCode'] = '';
            }
        }

        if (isset($this->request->post[$this->getPrefix().'capusta_merchant_token'])) {
            $data[$this->getPrefix().'capusta_merchant_token'] = $this->request->post[$this->getPrefix().'capusta_merchant_token'];
        } else {
            if ($this->config->get($this->getPrefix().'capusta_merchant_token')) {
                $data[$this->getPrefix().'capusta_merchant_token'] = $this->config->get($this->getPrefix().'capusta_merchant_token');
            } else {
                $data[$this->getPrefix().'capusta_merchant_token'] = '';
            }
        }

        if (isset($this->request->post[$this->getPrefix().'capusta_order_status_progress_id'])) {
            $data[$this->getPrefix().'capusta_order_status_progress_id'] = $this->request->post[$this->getPrefix().'capusta_order_status_progress_id'];
        } elseif ($this->config->has($this->getPrefix().'capusta_order_status_progress_id')) {
            $data[$this->getPrefix().'capusta_order_status_progress_id'] = $this->config->get($this->getPrefix().'capusta_order_status_progress_id');
        } else {
            $data[$this->getPrefix().'capusta_order_status_progress_id'] = '1';
        }

        if (isset($this->request->post[$this->getPrefix().'capusta_order_status_success_id'])) {
            $data[$this->getPrefix().'capusta_order_status_success_id'] = $this->request->post[$this->getPrefix().'capusta_order_status_success_id'];
        } elseif ($this->config->has($this->getPrefix().'capusta_order_status_success_id')) {
            $data[$this->getPrefix().'capusta_order_status_success_id'] = $this->config->get($this->getPrefix().'capusta_order_status_success_id');
        } else {
            $data[$this->getPrefix().'capusta_order_status_success_id'] = '2';
        }


        if (isset($this->request->post[$this->getPrefix().'capusta_order_status_progress_id'])) {
            $data[$this->getPrefix().'capusta_order_status_progress_id'] = $this->request->post[$this->getPrefix().'capusta_order_status_progress_id'];
        } elseif ($this->config->has($this->getPrefix().'capusta_order_status_progress_id')) {
            $data[$this->getPrefix().'capusta_order_status_progress_id'] = $this->config->get($this->getPrefix().'capusta_order_status_progress_id');
        } else {
            $data[$this->getPrefix().'capusta_order_status_progress_id'] = '1';
        }


        if (isset($this->request->post[$this->getPrefix().'capusta_order_status_cancelled_id'])) {
            $data[$this->getPrefix().'capusta_order_status_cancelled_id'] = $this->request->post[$this->getPrefix().'capusta_order_status_cancelled_id'];
        } elseif ($this->config->has($this->getPrefix().'capusta_order_status_cancelled_id')) {
            $data[$this->getPrefix().'capusta_order_status_cancelled_id'] = $this->config->get($this->getPrefix().'capusta_order_status_cancelled_id');
        } else {
            $data[$this->getPrefix().'capusta_order_status_cancelled_id'] = '7';
        }


        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();


        if (isset($this->request->post[$this->getPrefix().'capusta_button_label'])) {
            $data[$this->getPrefix().'capusta_button_label'] = $this->request->post[$this->getPrefix().'capusta_button_label'];
        } else {
            if ($this->config->get($this->getPrefix().'capusta_button_label')) {
                $data[$this->getPrefix().'capusta_button_label'] = $this->config->get($this->getPrefix().'capusta_button_label');
            } else {
                $data[$this->getPrefix().'capusta_button_label'] = $this->language->get('button_confirm');;
            }
        }

        if (isset($this->request->post[$this->getPrefix().'capusta_geo_zone_id'])) {
            $data[$this->getPrefix().'capusta_geo_zone_id'] = $this->request->post[$this->getPrefix().'capusta_geo_zone_id'];
        } else {
            $data[$this->getPrefix().'capusta_geo_zone_id'] = $this->config->get($this->getPrefix().'capusta_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();


        if (isset($this->request->post[$this->getPrefix().'capusta_status'])) {
            $data[$this->getPrefix().'capusta_status'] = $this->request->post[$this->getPrefix().'capusta_status'];
        } else {
            $data[$this->getPrefix().'capusta_status'] = $this->config->get($this->getPrefix().'capusta_status');
        }

        if (isset($this->request->post[$this->getPrefix().'capusta_sort_order'])) {
            $data[$this->getPrefix().'capusta_sort_order'] = $this->request->post[$this->getPrefix().'capusta_sort_order'];
        } else {
            $data[$this->getPrefix().'capusta_sort_order'] = $this->config->get($this->getPrefix().'capusta_sort_order');
        }

        $data['notify_url'] = HTTPS_CATALOG . 'index.php?route=extension/payment/capusta/callback';
        $data['success_url'] = HTTPS_CATALOG . 'index.php?route=checkout/success';
        $data['fail_url'] = HTTPS_CATALOG . 'index.php?route=checkout/checkout';

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/payment/capusta', $data));
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'extension/payment/capusta')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }


        if (!$this->request->post[$this->getPrefix().'capusta_merchant_email']) {
            $this->error['merchant_email'] = $this->language->get('error_merchant_email');
        }

        if (!$this->request->post[$this->getPrefix().'capusta_merchant_projectCode']) {
            $this->error['merchant_projectCode'] = $this->language->get('error_merchant_projectCode');
        }

        if (!$this->request->post[$this->getPrefix().'capusta_merchant_token']) {
            $this->error['merchant_token'] = $this->language->get('error_merchant_token');
        }

        return !$this->error;
    }
}

class ControllerPaymentCapusta extends ControllerExtensionPaymentCapusta
{
}